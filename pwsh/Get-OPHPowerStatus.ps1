#!/usr/bin/pwsh

<#
.SYNOPSIS
    Name: Get-OPHPowerStatus.ps1
    The purpose of this script is to gather the power status of
    each OPF Hub machine via IPMI.
  
.DESCRIPTION
    Each host in the Hub is queried via IPMI for its chassis power
    status. The results are then table-ized and displayed.

.PARAMETER Credential
    [Required] - The credentials used to query the OpenBMC for the 
    chassis power status.

.PARAMETER Hostname
    [Optional] - The hostname of a single Hub system to query the 
    chassis power status of.

.NOTES
    Version: 1.0
    Updated: Aug 2, 2022

    Author:
    Maximillian Schmidt - FRA - Computational Scientist
    Oregon State University - CQLS + OSL
    max@cqls.oregonstate.edu
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $false)][PSCredential] $Credential,
    [Parameter(Mandatory = $false)][string] $Hostname
)

# Default number of hosts
$testRange = 1..12

$oobCreds = $null

if ($Credential)
{
    $oobCreds = $Credential
}
else
{
    $oobCreds = Get-Credential
}

$user = $oobCreds.UserName
[string]$pass = $oobCreds.GetNetworkCredential().Password
$results = [ordered]@{}

if ($Hostname)
{
    $result = (ipmitool -I lanplus -C 17 -H "$Hostname" -U "$user" -P "$pass" chassis power status | Out-String).split(' ')[-1].split("`n")[0]

    if ($result -eq 'off' -or $result -eq 'on')
    {
        $results.Add($hostname,$result)
        Write-Host '.' -NoNewline
    }
    else
    {
        Write-Host ""
        Write-Error "Unknown power state ($result) for $hostname!"
        $results.Add($hostname,"unknown")
    }
}
else
{
    Write-Host "`n  Gathering results from hosts. Please wait." -NoNewline

    # Get each of the OPH machine's power status
    $testRange | ForEach-Object {
        $hostname = "oph$($_).osuosl.oob"

        $result = (ipmitool -I lanplus -C 17 -H "$hostname" -U "$user" -P "$pass" chassis power status | Out-String).split(' ')[-1].split("`n")[0]

        if ($result -eq 'off' -or $result -eq 'on')
        {
            $results.Add($hostname,$result)
            Write-Host '.' -NoNewline
        }
        else
        {
            Write-Host ""
            Write-Error "Unknown power state ($result) for $hostname!"
            $results.Add($hostname,"unknown")
        }
    }

Write-Host ""
}


# Colorized table - https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences#text-formatting
$results | Format-Table Name,@{
    Label = "Status"
    Expression =
    {
        switch ($_.Value) # Switch on the hash table item's value
        {
            'off' { $color = "31"; break }      # red
            'on' { $color = "32"; break }       # green
            'unknown' { $color = "33"; break }  # yellow
            default { $color = "0" }
        }
        $e = [char]27
        "$e[${color}m$($_.Value)${e}[0m"
    }
}